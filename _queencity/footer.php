<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package _queencity
 */
?>

	</div><!-- #content -->
<?php
if(is_page('home')||is_page('23')) {
	// EmptyIterator
}
else {
?>
<div class="row footerRow footerBorder">
    	<div class="small-12 columns" id="socialSection">
		<img src="<?php echo get_template_directory_uri(); ?>/images/socialHeart.svg" alt="Stay in Touch with Queen City Paper Company">
		<h4>Stay in Touch</h4>
	 <?php echo do_shortcode('[mc4wp_form]'); ?>
		<a href="faceook page">
			<span class="icon-facebook-circled"></span>
		</a>
		<a href="pinterest page">
			<span class="icon-pinterest-circled"></span>
		</a>
		<a href="twitter page">
			<span class="icon-twitter-circled"></span>
		</a>
	</div>
</div>
<?php
    }

?>
    <div class="row footerRow">
	<div class="small-12 columns">
		<footer id="bottomNav" class="site-footer" role="contentinfo">
			<div class="site-info">
				<?php wp_nav_menu( array( 'theme_location' => 'footer' ) ); ?>
			</div><!-- .site-info -->
			<div id="colophon">
			<p>&copy; <?php echo date('Y'); ?> Queen City Paper Co. All rights reserved.</p>
			<p><a href="<?php echo get_page_link('104'); ?>">Terms of Service</a> | <a href="<?php echo get_page_link('106'); ?>">Privacy Policy</a></p>
				<p>Wedding Invitations - Greeting Cards - Holiday Cards - Notebooks - Notepads - Stationery - And More
				</p>
			</div>
		</footer><!-- #colophon -->
	</div>
	</div>
</div><!-- #page -->

<?php wp_footer(); ?>
	<script>
	  jQuery(document).foundation();
	</script>
</body>
</html>
