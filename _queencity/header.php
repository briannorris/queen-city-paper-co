<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package _queencity
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', '_queencity' ); ?></a>
	<?php
		if (is_page( 'landing-page' )) {
		}
		else {
	?>

		<header id="masthead" class="site-header" role="banner">
			<div class="row">
				<div class="small-12 medium-6 columns">
					<div class="site-branding">
						<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_template_directory_uri(); ?>/images/QCRLogo.svg" alt="Queen City Paper Company" width="238" height="120"></a></h1>
					</div>
				</div>
				<div class="small-12 medium-6 columns" id="headerRight">
					<?php get_search_form(); ?>
					<table id="metaNav">
						<tr>
						<td id="account">
							<a href="<?php echo get_page_link('7'); ?>">My Account</a>
						</td>
						<td id="wish">
							<a href="<?php echo get_page_link('44'); ?>">Wish List</a>
						</td>
						<td id="basket">
							<a href="<?php echo get_page_link('5'); ?>">Shopping Basket</a>
						</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
									<!--<nav id="site-navigation" class="main-navigation" role="navigation">-->
					<nav id="site-navigation" class="" role="navigation">
						<!--<button class="menu-toggle"><?php //_e( 'Primary Menu', '_queencity' ); ?></button>-->
						<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
					</nav><!-- #site-navigation -->
				</div>
			</div>
		</header><!-- #masthead -->
	</div>
	</div>
	<?php
		}
	?>
	<div id="content" class="site-content">
