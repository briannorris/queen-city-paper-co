<?php
/**
 * Template Name: Landing Page
 *
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<div class="row">
				<div class="small-12 medium-10 large-8 small-centered columns">
					<div id="landingWrapper">
						<img width="300" class="logo" src="<?php echo Get_template_directory_uri(); ?>/images/QCPaperCoShield.svg" alt="Queen City Paper Company Springfield, Missouri">
						<h2>Opening 2016</h2>
						<h4>Join our mailing list for news of our grand opening.</h4>
						<div id="contactForm" class="row">
							<!--<div class="small-12 medium-6 columns">
								<button id="cFormButton" class="e">Send Me a Note</button>
								<div id="formContainer" class="e">
									<?php //echo do_shortcode( '[contact-form-7 id="25" title="Landing Page Contact Form"]' ); ?>
								</div>
							</div>-->
							<div class="small-12 medium-6 medium-centered columns">
								<!--<button id="cFormButton" class="l">Join Our Mailing List</button>-->
								<div id="formContainer" class="l">
									<?php echo do_shortcode('[mc4wp_form]'); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php //get_sidebar(); ?>
<?php //get_footer(); ?>
