<?php
/**
 * _queencity functions and definitions
 *
 * @package _queencity
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( '_queencity_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function _queencity_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on _queencity, use a find and replace
	 * to change '_queencity' to the name of your theme in all the template files
	 */
	load_theme_textdomain( '_queencity', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', '_queencity' ),
		//'category' => __( 'Category Menu', '_queencity' ),
		'footer' => __( 'Footer Menu', '_queencity' ),
	) );

	// Enable support for Post Formats.
	add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );

	// Setup the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( '_queencity_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Enable support for HTML5 markup.
	add_theme_support( 'html5', array(
		'comment-list',
		'search-form',
		'comment-form',
		'gallery',
		'caption',
	) );
}
endif; // _queencity_setup
add_action( 'after_setup_theme', '_queencity_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function _queencity_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', '_queencity' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', '_queencity_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function _queencity_scripts() {
	wp_enqueue_style( '_queencity-style', get_stylesheet_uri() );

	wp_enqueue_style( 'foundation-normal', get_template_directory_uri() . '/css/normalize.css' );
	wp_enqueue_style( 'foundation', get_template_directory_uri() . '/css/foundation.css' );
	wp_enqueue_style( 'custom', get_template_directory_uri() . '/css/custom.less' );
	wp_enqueue_style( 'entypo', get_template_directory_uri() . '/css/entypo.css' );

	wp_enqueue_script( '_queencity-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( '_queencity-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/vendor/modernizr.js', array(), '20140415', false );

	wp_enqueue_script( 'foundation', get_template_directory_uri() . '/js/foundation.min.js', array(), '20140415', true );

	wp_enqueue_script( 'custom', get_template_directory_uri() . '/js/custom.js', array(), '1', true );




	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', '_queencity_scripts' );

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


/* KILL ALL WOOCOMMERCE STYLES */
add_filter( 'woocommerce_enqueue_styles', '__return_false' );

/* MORE WOOCOMMERCE */
//Reposition WooCommerce breadcrumb
function woocommerce_remove_breadcrumb(){
	remove_action(
    	'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
}
add_action(
    'woocommerce_before_main_content', 'woocommerce_remove_breadcrumb'
);

function woocommerce_custom_breadcrumb(){
    woocommerce_breadcrumb();
}

add_action( 'woo_custom_breadcrumb', 'woocommerce_custom_breadcrumb' );


// Rearrange product page
/**
			 * woocommerce_single_product_summary hook
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_rating - 10
			 * @hooked woocommerce_template_single_price - 10
			 * @hooked woocommerce_template_single_excerpt - 20
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 */

// TITLE
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );

function woocommerce_custom_title() {
	woocommerce_template_single_title();
}

add_action('woo_custom_title', 'woocommerce_custom_title');


// Price
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );

function woocommerce_custom_price() {
	woocommerce_template_single_price();
}

add_action('woo_custom_price', 'woocommerce_custom_price');


// Meta
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

function woocommerce_custom_meta() {
	woocommerce_template_single_meta();
}

add_action('woo_custom_meta', 'woocommerce_custom_meta');


// Add To Cart
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );

function woocommerce_custom_add_to_cart() {
	woocommerce_template_single_add_to_cart();
}

add_action('woo_custom_add_to_cart', 'woocommerce_custom_add_to_cart');

		/**
		 * woocommerce_after_single_product_summary hook
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_output_related_products - 20
		 */

// Description
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

function woocommerce_custom_description() {
	woocommerce_output_product_data_tabs();
}

add_action('woo_custom_description', 'woocommerce_custom_description');

// Related Products
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

function woocommerce_custom_related_products() {
	woocommerce_output_related_products();
}

add_action('woo_custom_related', 'woocommerce_custom_related_products');


/**
	 * woocommerce_before_single_product hook
	 *
	 * @hooked wc_print_notices - 10
	 */

// Alerts
remove_action('woocommerce_before_single_product', 'wc_print_notices', 10 );

function woocommerce_custom_print_notices() {
	wc_print_notices();
}

add_action('woo_custom_print_notices', 'woocommerce_custom_print_notices');


/* PUTS NOCLICK CLASS ON LINK WHERE Link Relationship (XFN) IS NOCLICK IN WP MENUS */
function add_menuclass($ulclass) {
return preg_replace('/<a rel="noclick"/', '<a rel="noclick" class="noclick"', $ulclass, 1);
}
add_filter('wp_nav_menu','add_menuclass');

// Display 12 products per page. Goes in functions.php
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 12;' ), 20 );
