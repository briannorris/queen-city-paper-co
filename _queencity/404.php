<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package _queencity
 */

get_header(); ?>
<div class="row">
	<div class="small-12 medium-3 columns"><?php get_sidebar(); ?></div>
	<div class="small-12 medium-9 columns">

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', '_queencity' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', '_queencity' ); ?></p>

					<?php get_search_form(); ?>

					<?php the_widget( 'WP_Widget_Recent_Posts' ); ?>

					<?php //if ( _queencity_categorized_blog() ) : // Only show the widget if site has multiple categories. ?>
					<!--<div class="widget widget_categories">
						<h2 class="widgettitle"><?php //_e( 'Most Used Categories', '_queencity' ); ?></h2>
						<ul>
						<?php
							/*wp_list_categories( array(
								'orderby'    => 'count',
								'order'      => 'DESC',
								'show_count' => 1,
								'title_li'   => '',
								'number'     => 10,
							) );*/
						?>
						</ul>
					</div> -->
					<?php //endif; ?>

					<?php
						/* translators: %1$s: smiley */
						//$archive_content = '<p>' . sprintf( __( 'Try looking in the monthly archives. %1$s', '_queencity' ), convert_smilies( ':)' ) ) . '</p>';
						//the_widget( 'WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$archive_content" );
					?>

					<?php //the_widget( 'WP_Widget_Tag_Cloud' ); ?>

				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->
</div>
</div>
<?php get_footer(); ?>
