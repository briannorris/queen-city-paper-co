<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>


	<?php
		get_header( 'shop' );
	?>

<div class="row">
	<div class="small-12 columns">
		<?php do_action('woo_custom_breadcrumb'); ?>
	</div>
</div>


<div class="row">
	<div class="small-12 medium-3 columns">
		<?php
		/**
		 * woocommerce_sidebar hook
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		do_action( 'woocommerce_sidebar' );
	?>
	</div>
	<div class="small-12 medium-5 columns">
	<?php
		/**
		 * woocommerce_before_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>

		<?php while ( have_posts() ) : the_post(); ?>

			<?php wc_get_template_part( 'content', 'single-product' ); ?>

		<?php endwhile; // end of the loop. ?>



	<?php
		/**
		 * woocommerce_after_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>

	</div>
	<div class="small-12 medium-4 columns">
		<?php do_action('woo_custom_title'); ?>
		<?php do_action('woo_custom_price'); ?>
		<?php do_action('woo_custom_description'); ?>
		<?php do_action('woo_custom_meta'); ?>
		<?php do_action('woo_custom_add_to_cart'); ?>
		<?php do_action('woo_custom_print_notices'); ?>
	</div>
</div>
<div class="row">
	<div class="small-12 medium-3 columns"></div>
	<div class="small-12 medium-9 columns">
		<?php do_action('woo_custom_related'); ?>
	</div>
</div>

<?php get_footer( 'shop' ); ?>
