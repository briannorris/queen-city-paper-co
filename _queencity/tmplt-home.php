<?php
/**
 * Template Name: Home Page
 *
 */
get_header(); ?>
<div class="row" id="topSection">
	<div class="small-12 medium-3 columns">
		<?php
			get_sidebar();
		?>
	</div>
	<div class="small-12 medium-9 columns">
		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">
				<div class="row">
				<!-- Featured Image Large -->
				<?php

				// Build the Query
				$args = array(
					'post_type' => 'featured_image',
					'featured_image_type' => 'large'
					);

				$pic_query = new WP_Query( $args );

				if ( $pic_query->have_posts() ) :

					while ( $pic_query->have_posts() ) : $pic_query->the_post();
						?>
						<div class="small-12 columns featuredImage">
						<?php
						echo get_the_post_thumbnail( $post->ID );
						?>
						</div>
						<?php
					endwhile;

				endif;

				?>
				</div>


				<div class="row">
					<!-- Small Featured Image Large -->
					<?php

					// Build the Query
					$args = array(
						'post_type' => 'featured_image',
						'featured_image_type' => 'small'
						);
					$spic_query = new WP_Query( $args );

					if ( $spic_query->have_posts() ) :
						while ( $spic_query->have_posts() ) : $spic_query->the_post();

					?>
						<div class="small-12 medium-6 columns smallFeaturedImage">
							<!-- featured image -->
							<?php echo get_the_post_thumbnail( $post->ID ); ?>
						</div>
					<?php
						endwhile;
					endif;
					?>
				</div>

			</main><!-- #main -->
		</div><!-- #primary -->
	</div>
</div>
<div class="row footerBorder" id="bottomSection">

	<div class="small-12 large-9 large-push-3 columns" id="homeContent">
		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'content', 'page' ); ?>

			<?php
				// If comments are open or we have at least one comment, load up the comment template
				if ( comments_open() || '0' != get_comments_number() ) :
					comments_template();
				endif;
			?>

		<?php endwhile; // end of the loop. ?>
	</div>

	<div class="small-12 large-3 large-pull-9 columns" id="socialSection">
		<img src="<?php echo get_template_directory_uri(); ?>/images/socialHeart.svg" alt="Stay in Touch with Queen City Paper Company">
		<h4>Stay in Touch</h4>
	 <?php echo do_shortcode('[mc4wp_form]'); ?>
		<a href="faceook page">
			<span class="icon-facebook-circled"></span>
		</a>
		<a href="pinterest page">
			<span class="icon-pinterest-circled"></span>
		</a>
		<a href="twitter page">
			<span class="icon-twitter-circled"></span>
		</a>
	</div>
</div>

<?php get_footer(); ?>
